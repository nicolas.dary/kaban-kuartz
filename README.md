# kanban

> Test technique du kaban | Kuartz

## Build Setup

``` bash
# install dependencies
yarn install

# serve with hot reload at localhost:8080
yarn run dev

# build for production with minification
yarn run build
```

For detailed explanation on how things work, consult the [docs for vue-loader](http://vuejs.github.io/vue-loader).

## Dépendances

Ce kanban utilise vue-kanban, axios, airtable et vue.js.

## Etat actuel

Malheureusement ce kanban ne fonctionne pour l'instant qu'avec la commande :
``` bash
yarn run dev
```
en effet la commande 
``` bash
yarn run build
```
ne fonctionne pas encore pour des raisons que je n'ai pas encore découvertes.

[Site hébergé sur netlify](https://adoring-almeida-98dcf4.netlify.app/ "Site hébergé sur netlify")
