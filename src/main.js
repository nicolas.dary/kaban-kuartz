import Vue from 'vue'
import App from './App.vue'
import vueKanban from 'vue-kanban'

Vue.use(vueKanban)

new Vue({
  el: '#app',
  render: h => h(App)
})



